#############
Prerequisites
#############

.. contents:: Table of Contents

----------

Downloading the Package Archive
********************************

The AMDGPU graphics stack is delivered as an archive of native
packages. Prior to installation, you must first obtain the archive from
your AMD Customer Engagement Representative or from the `AMD web site <https://support.amd.com/en-us/download>`_.

Download the AMDGPU-Pro tar archive (for example, to ``~/Downloads``),
which contains the installation script.

--------

Extracting the Package Archive
*******************************

Extract the tar archive to a known location. In the following commands,
replace ``YY.XX`` with the actual release number (e.g. 18.30) and ``NNNNNN``
six digit build number of the downloaded file:

.. code-block:: bash

 $ cd ~/Downloads
 $ tar -Jxvf amdgpu-pro-YY.XX-NNNNNN.tar.xz
 $ cd ~/Downloads/amdgpu-pro-YY.XX-NNNNNN

---------

Configuring Access to the Distribution Repository (RHEL and SLE only)
**********************************************************************

AMDGPU stack depends on packages provided by the Linux distribution vendors.

The AMDGPU-Pro driver requires access to specific RPMs from Red Hat
Enterprise Linux (RHEL) or SUSE Linux Enterprise (SLE) installation media
for the purpose of dependency resolution. You must ensure
**one** of the following:

- Have a valid subscription and be connected to Internet during installation.
- Mount an installation media (for example, DVD, USB key or ISO file). Media
  mounting instructions for **Red Hat** systems are provided at
  https://access.redhat.com/solutions/1355683. For **SLE**, use YaST to add the
  installation media as a new repository, see `the SUSE documentation
  <https://documentation.suse.com/sles/15-SP1/html/SLES-all/cha-deployment-instserver.html#sec-remote-installation-iso>`_
  for details.

.. note:: In previous releases (before 18.30), a script named
      ``amdgpu-pro-preinstall.sh`` was provided to carry out this step and
      to perform other pre-installation checks and configuration. Starting
      with 18.30, all external dependencies (excluding dependencies on
      packages provided by the Linux distribution vendor) have been
      eliminated and the checks have been integrated into
      ``amdgpu-install``. Consequently, ``amdgpu-pro-preinstall.sh`` has
      been eliminated to streamline installation. However, access to
      distribution media or online package repositories is still required
      to satisfy base operating system dependencies. It is assumed most
      customers have a subscription with access to online repositories or
      are using a Linux distribution which does not require a subscription
      and has online repositories enabled by default.
